import express, { NextFunction, Request, Response } from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import authRoutes from './routes/auth';
import instaRoutes from './routes/insta';

const app = express();

app.use(bodyParser.json());

// This middleware enables CORS(Cross-Origin Resource Sharing)
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods',
    'GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.use((error: any, req: Request, res: Response, next: NextFunction) => {
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  const data = error.data;
  res.status(status).json({message: message, data: data});
});

app.use('/auth', authRoutes);
app.use('/insta', instaRoutes);

mongoose.connect(
  'mongodb+srv://sandro:1w9LpcPgEajuE8A6@cluster0.zdel9.mongodb.net/test?retryWrites=true&w=majority',
  {useNewUrlParser: true, useUnifiedTopology: true},
)
  .then(() => {
    app.listen(8080);
  })
  .catch(err => {
    console.log(err);
  });
