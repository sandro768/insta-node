import { NextFunction, Request, Response } from 'express';
import Insta from '../models/insta';

export const savePost = (req: Request, res: Response, next: NextFunction) => {
  Insta.findOne({username: req.body.username})
    .then(inst => {
      // Proceed if not exists already
      if (inst) {
        const error: any = new Error('Already exists');
        error.statusCode = 401;
        throw error;
      }

      const insta = new Insta({
        picture: req.body.picture,
        username: req.body.username,
        mediaCount: req.body.mediaCount,
        type: req.body.type,
      });
      insta
        .save()
        .then(() => {
          return Insta.find();
        })
        .then((result) => {
          res.status(201).json({
            message: 'Created the insta entry. Returning updated table',
            insta: result,
          });
        })
        .catch(err => {
          if (!err.statusCode) {
            err.statusCode = 500;
          }
          next(err);
        });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

export const getAll = async (
  req: Request, res: Response, next: NextFunction) => {
  try {
    const result = await Insta.find();
    res.status(200).json({message: 'Fetched successfully', result});
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

export const deleteInsta = async (
  req: Request, res: Response, next: NextFunction) => {
  const instaId = req.params.instaId;
  const insta = await Insta.findById(req.params.instaId);
  if (!insta) {
    const error: any = new Error('Could not fnd insta');
    error.statusCode = 404;
    throw error;
  }
  await Insta.findByIdAndRemove(instaId);
  const result = await Insta.find();
  res.status(201).json({
    message: 'Removed the insta entry. Returning updated table',
    insta: result,
  });
};
