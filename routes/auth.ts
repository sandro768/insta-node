import express from 'express';
import { body } from 'express-validator/check';
import User from '../models/user';
import { singup, signin } from '../controllers/auth';

const router = express.Router();

router.put('/signup', [
  body('email')
    .isEmail()
    .withMessage('Please enter a valid email.')
    .custom((value, {req}) => {
      return User.findOne({email: value}).then(userDoc => {
        if (userDoc) {
          return Promise.reject('Email address already exists!');
        }
      });
    })
    .normalizeEmail(),
  body('name').trim().notEmpty(),
  body('password').trim().notEmpty(),
], singup);

router.post('/signin', signin)

export default router;
