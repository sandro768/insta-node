import express from 'express';
import { deleteInsta, getAll, savePost } from '../controllers/insta';
import isAuth from '../middleware/is-auth';

const router = express.Router();

router.post('/save', isAuth, savePost);
router.get('/all', isAuth, getAll);
router.delete('/delete/:instaId', isAuth, deleteInsta);

export default router;
