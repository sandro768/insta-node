import mongoose from 'mongoose';
import { IUser } from './user';

const instaSchema = new mongoose.Schema({
  picture: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  mediaCount: {
    type: String,
  },
  type: {
    type: String,
    required: true
  }
}, {timestamps: true})

export default mongoose.model<IUser>('Insta', instaSchema)
