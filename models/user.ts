import mongoose, { Document } from 'mongoose';

export interface IUser extends Document {
  password: string
}

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
}, {timestamps: true});

export default mongoose.model<IUser>('User', userSchema);
